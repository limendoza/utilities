package com.limendoza.utilities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Results of word validation
 * @author Lorenzo Iraj Mendoza
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ValidationResultDTO {

    /**
     * Validation result
     */
    private boolean valid;

    /**
     * Errors related with the validation result
     */
    private List<FieldErrorDTO> errorFields = new ArrayList<>();

    /**
     * Error related with the validation result in a single string
     */
    private String error;

    public ValidationResultDTO(boolean valid) {
        this.valid = valid;
    }

    public String getError() {
        return errorFields.isEmpty() ? "No errors"
                : errorFields.stream().map(FieldErrorDTO::getError).collect(Collectors.joining(", "));
    }
}
