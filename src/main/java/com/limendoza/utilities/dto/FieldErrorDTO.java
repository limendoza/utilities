package com.limendoza.utilities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Relates the cause of validation error
 * @author Lorenzo Iraj Mendoza
 */
@Getter
@Setter
@AllArgsConstructor
public class FieldErrorDTO {
    /**
     * Error code
     */
    private String code;

    /**
     * Error field
     */
    private String field;

    /**
     * Information about the error
     */
    private String error;

    /**
     * Rejected value that causes the error
     */
    private Object rejectedValue;
}
