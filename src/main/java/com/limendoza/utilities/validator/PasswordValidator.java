package com.limendoza.utilities.validator;

import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.intr.RuleValidator;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

/**
 * Validate if the password meets the defined rules
 * @author Lorenzo Iraj Mendoza
 */
public class PasswordValidator implements RuleValidator {

    private static final String CODE_EMPTY = ".empty";
    private static final String EMPTY_ERROR = "Empty password input";

    private List<RuleValidator> ruleValidators;

    public PasswordValidator(List<RuleValidator> ruleValidators) {
        this.ruleValidators = ruleValidators;

        if (ruleValidators.isEmpty()) {
            throw new IllegalArgumentException("Rules must be defined for validation");
        }
    }

    public ValidationResultDTO validate(String word) {
        return validate("password", word);
    }

    @Override
    public ValidationResultDTO validate(String field, String word) {
        ValidationResultDTO validationResultDTO = new ValidationResultDTO(true);

        if (StringUtils.isEmpty(word)) {
            FieldErrorDTO emptyPasswordField = new FieldErrorDTO(field.concat(CODE_EMPTY), field, EMPTY_ERROR, word);
            return new ValidationResultDTO(false, Collections.singletonList(emptyPasswordField), emptyPasswordField.getError());
        } else {
            ruleValidators.forEach(ruleValidator -> {
                ValidationResultDTO definedRuleResults = ruleValidator.validate(field, word);
                if (!definedRuleResults.isValid()) {
                    validationResultDTO.setValid(false);
                    validationResultDTO.getErrorFields().addAll(definedRuleResults.getErrorFields());
                }
            });
        }

        return validationResultDTO;
    }
}
