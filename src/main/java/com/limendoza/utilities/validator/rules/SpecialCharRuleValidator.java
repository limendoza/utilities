package com.limendoza.utilities.validator.rules;

import org.springframework.util.StringUtils;

/**
 * Validate if the word contains a number of special characters
 * @author Lorenzo Iraj Mendoza
 */
public class SpecialCharRuleValidator extends CharacterRuleValidator {

    private String specialChars;
    private static final String MIN_CHAR_LENGTH = "Minimum of %d special character";

    public SpecialCharRuleValidator(int min, String specialChars) {
        setMin(min);
        setError(MIN_CHAR_LENGTH);
        this.specialChars = specialChars;

        if (min <= 0) {
            throw new IllegalArgumentException("Minimum must be greater than 0");
        } else if (StringUtils.isEmpty(specialChars)) {
            throw new IllegalArgumentException("Empty special characters");
        }
    }

    public boolean isValidCharCase(char character) {
        String regex = String.format("[%s]", specialChars);
        return String.valueOf(character).matches(regex);
    }

}
