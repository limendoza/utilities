package com.limendoza.utilities.validator.rules;

import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.intr.RuleValidator;

/**
 * Validate if the word character sequence is immediately followed by the same sequence
 * @author Lorenzo Iraj Mendoza
 */
public class SequenceRuleValidator implements RuleValidator {

    private static final String CODE_INVALID = ".invalid";
    private static final String REPEATED_SEQUENCE = "%s is a repeated sequence";

    @Override
    public ValidationResultDTO validate(String field, String word) {
        ValidationResultDTO validationResultDTO = new ValidationResultDTO();

        boolean isRepeated = false;
        String repeatedSequence = null;

        for (int i = 0; i < word.length(); i++) {
            for (int j = i +1; j < word.length(); j++) {
                if (word.charAt(i) == word.charAt(j)) {
                    int lastIndex = j + j - i;
                    if (lastIndex <= word.length() && !isRepeated) {
                        isRepeated = word.substring(j, lastIndex).equals(word.substring(i, j));
                        repeatedSequence = word.substring(i, j);
                    }
                }
            }

            if (isRepeated) {
                break;
            }
        }

        if (isRepeated) {
            validationResultDTO.setValid(false);
            validationResultDTO.getErrorFields().add(new FieldErrorDTO(field.concat(CODE_INVALID), field,
                    String.format(REPEATED_SEQUENCE, repeatedSequence), word));
        } else {
            validationResultDTO.setValid(true);
        }

        return validationResultDTO;
    }
}
