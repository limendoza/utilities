package com.limendoza.utilities.validator.rules;

import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.intr.RuleValidator;
import org.springframework.util.StringUtils;

/**
 * Validates if the word is contains a invalid character based on the defined set of characters
 * @author Lorenzo Iraj Mendoza
 */
public class InvalidCharRuleValidator implements RuleValidator {

    private String invalidChars;

    private static final String CODE_INVALID = ".invalid";
    private static final String INVALID_CHAR_ERROR = "Invalid characters %s";

    public InvalidCharRuleValidator(String invalidChars) {
        this.invalidChars = invalidChars;

        if (StringUtils.isEmpty(invalidChars)) {
            throw new IllegalArgumentException("Invalid Characters must be defined");
        }
    }

    @Override
    public ValidationResultDTO validate(String field, String word) {
        ValidationResultDTO validationResultDTO = new ValidationResultDTO(true);
        String regex = String.format(".*[%s].*", invalidChars);

        if (word.matches(regex)) {
            validationResultDTO.setValid(false);
            validationResultDTO.getErrorFields()
                    .add(new FieldErrorDTO(field.concat(CODE_INVALID), field, String.format(INVALID_CHAR_ERROR, invalidChars), word));
        }

        return validationResultDTO;
    }
}
