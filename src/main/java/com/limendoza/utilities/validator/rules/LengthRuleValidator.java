package com.limendoza.utilities.validator.rules;

import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.intr.RuleValidator;

/**
 * Validates if the word is within the defined length
 * @author Lorenzo Iraj Mendoza
 */
public class LengthRuleValidator implements RuleValidator {

    private int minLength;
    private int maxLength;

    private static final String CODE_LENGTH = ".length";
    private static final String MIN_LENGTH_ERROR = "Minimum of %d characters";
    private static final String MAX_LENGTH_ERROR = "Maximum of %d characters";

    public LengthRuleValidator(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;

        if (maxLength < minLength) {
            throw new IllegalArgumentException("Maximum length must be higher than the minimum length");
        }
    }

    @Override
    public ValidationResultDTO validate(String field, String word) {
        ValidationResultDTO validationResultDTO = new ValidationResultDTO();
        int length = word.length();

        if (length >= minLength && length <= maxLength) {
            validationResultDTO.setValid(true);
        } else {
            validationResultDTO.setValid(false);
            if (length < minLength) {
                validationResultDTO.getErrorFields()
                        .add(new FieldErrorDTO(field.concat(CODE_LENGTH), field, String.format(MIN_LENGTH_ERROR, minLength), word));
            }
            if (length > maxLength) {
                validationResultDTO.getErrorFields()
                        .add(new FieldErrorDTO(field.concat(CODE_LENGTH), field, String.format(MAX_LENGTH_ERROR, maxLength), word));
            }
        }

        return validationResultDTO;
    }
}
