package com.limendoza.utilities.validator.rules;

/**
 * Validates if the word contains a number of uppercase characters
 * @author Lorenzo Iraj Mendoza
 */
public class UpperCaseRuleValidator extends CharacterRuleValidator {

    private static final String MIN_CHAR_LENGTH = "Minimum of %d upper case character";

    public UpperCaseRuleValidator(int min) {
        setMin(min);
        setError(MIN_CHAR_LENGTH);

        if (min <= 0) {
            throw new IllegalArgumentException("Minimum must be greater than 0");
        }
    }

    public boolean isValidCharCase(char character) {
        return Character.isUpperCase(character);
    }
}
