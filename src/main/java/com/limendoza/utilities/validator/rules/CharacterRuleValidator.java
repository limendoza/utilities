package com.limendoza.utilities.validator.rules;

import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.intr.RuleValidator;

/**
 * Validates if the word contains a number of character
 * @author Lorenzo Iraj Mendoza
 */
public abstract class CharacterRuleValidator implements RuleValidator {

    private int min;
    private String error;
    private static final String CODE_INVALID = ".invalid";

    public void setMin(int min) {
        this.min = min;
    }

    public void setError(String error) {
        this.error = String.format(error, min);
    }

    public abstract boolean isValidCharCase(char character);

    @Override
    public ValidationResultDTO validate(String field, String word) {
        ValidationResultDTO validationResultDTO = new ValidationResultDTO();
        int totalCharCase = 0;

        for (int i = 0; i < word.length(); i++) {
            if (isValidCharCase(word.charAt(i))) {
                totalCharCase++;
            }
        }

        if (totalCharCase > 0) {
            validationResultDTO.setValid(true);
        } else {
            validationResultDTO.setValid(false);
            validationResultDTO
                    .getErrorFields()
                    .add(new FieldErrorDTO(field.concat(CODE_INVALID), field, error, word));
        }

        return validationResultDTO;
    }

}
