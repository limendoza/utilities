package com.limendoza.utilities.validator.rules;

/**
 * Validate if the word contains a number of numeric digit characters
 * @author Lorenzo Iraj Mendoza
 */
public class NumericCharRuleValidator extends CharacterRuleValidator {

    private static final String MIN_CHAR_LENGTH = "Minimum of %d numeric digit character";

    public NumericCharRuleValidator(int min) {
        setMin(min);
        setError(MIN_CHAR_LENGTH);

        if (min <= 0) {
            throw new IllegalArgumentException("Minimum must be greater than 0");
        }
    }

    public boolean isValidCharCase(char character) {
        return Character.isDigit(character);
    }

}
