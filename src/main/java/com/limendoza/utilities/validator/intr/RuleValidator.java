package com.limendoza.utilities.validator.intr;

import com.limendoza.utilities.dto.ValidationResultDTO;

/**
 * Interface for rule validation
 * @author Lorenzo Iraj Mendoza
 */
public interface RuleValidator {

    /**
     * Validates the supplied word per requirement
     * @param field reference on where the error occurred
     * @param word to be validated
     * @return result of word validation
     */
    ValidationResultDTO validate(String field, String word);

}
