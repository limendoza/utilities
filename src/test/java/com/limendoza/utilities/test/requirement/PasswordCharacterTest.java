package com.limendoza.utilities.test.requirement;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class PasswordCharacterTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordCharacterTest.class);

    // Test Positive
    @Test
    public void testWithValidCharacters() {
        assertThat(isValid("Qw3RtyuI0"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithNullParam() {
        assertThat(isValid(null), is(equalTo(false)));
    }

    @Test
    public void testWithNoLowerCaseCharacter() {
        assertThat(isValid("QW3RTYUI0"), is(equalTo(false)));
    }

    @Test
    public void testWithNoUpperCaseCharacter() {
        assertThat(isValid("qw3rtyui0"), is(equalTo(false)));
    }

    @Test
    public void testWithNoNumericCharacter() {
        assertThat(isValid("QwERtyuIO"), is(equalTo(false)));
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = passwordRuleValidator.validate(word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
