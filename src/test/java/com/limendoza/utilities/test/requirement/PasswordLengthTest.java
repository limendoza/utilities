package com.limendoza.utilities.test.requirement;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class PasswordLengthTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordLengthTest.class);

    // Test Positive
    @Test
    public void testWithValidMinLength() {
        assertThat(isValid("Qw3tY"), is(equalTo(true)));
    }

    @Test
    public void testWithValidMaxLength() {
        assertThat(isValid("Qw3RtYUi0Pas"), is(equalTo(true)));
    }

    @Test
    public void testWithInBetweenMinMaxLength() {
        assertThat(isValid("QW3RTyUi0"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithInvalidMinLength() {
        assertThat(isValid("Qw3r"), is(equalTo(false)));
    }

    @Test
    public void testWithInvalidMaxLength() {
        assertThat(isValid("QW3RTyUi0Pasd"), is(equalTo(false)));
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = passwordRuleValidator.validate(word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
