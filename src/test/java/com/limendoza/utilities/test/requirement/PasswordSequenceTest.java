package com.limendoza.utilities.test.requirement;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.FieldErrorDTO;
import com.limendoza.utilities.dto.ValidationResultDTO;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class PasswordSequenceTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordSequenceTest.class);

    // Test Positive
    @Test
    public void testWithValidSequence() {
        assertThat(isValid("W0rd1"), is(equalTo(true)));
    }

    @Test
    public void testWithCharInBetweenSequence() {
        assertThat(isValid("W0rd1xW0rd1"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithSameSequence() {
        assertThat(isValid("W0rd1W0rd1"), is(equalTo(false)));
    }

    @Test
    public void testWithCharFollowedBySameSequence() {
        assertThat(isValid("xW0rd1W0rd1"), is(equalTo(false)));
    }

    @Test
    public void testWithSameSequenceFollowedByChar() {
        assertThat(isValid("W0rd1W0rd1x"), is(equalTo(false)));
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = passwordRuleValidator.validate(word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
