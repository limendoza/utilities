package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.SpecialCharRuleValidator;
import com.limendoza.utilities.validator.rules.UpperCaseRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SpecialCharRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpecialCharRuleValidatorTest.class);

    // Test Positive
    @Test
    public void testWithSingleSpecialCharacter() {
        assertThat(isValid("Qw#rT"), is(equalTo(true)));
    }

    @Test
    public void testWithMultipleSpecialCharacter() {
        assertThat(isValid("!w#rT^"), is(equalTo(true)));
    }

    @Test
    public void testWithAllSpecialCharacter() {
        assertThat(isValid("!@#$%^"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithNoSpecialCharacter() {
        assertThat(isValid("qweRTY123"), is(equalTo(false)));
    }

    @Test
    public void testWithOutOfScopeSpecialCharacter() {
        assertThat(isValid("qweRTY123<>?:"), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMinIsZero() {
        SpecialCharRuleValidator specialCharRuleValidatorTest = new SpecialCharRuleValidator(0, "!@#$%^");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithSpecialCharsIsEmpty() {
        SpecialCharRuleValidator specialCharRuleValidatorTest = new SpecialCharRuleValidator(1, "");
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = specialCharRuleValidator.validate("specialChar", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
