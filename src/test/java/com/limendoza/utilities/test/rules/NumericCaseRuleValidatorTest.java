package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.NumericCharRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class NumericCaseRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumericCaseRuleValidatorTest.class);

    // Test Positive
    @Test
    public void testWithSingleNumericDigitCharacter() {
        assertThat(isValid("qw3rty"), is(equalTo(true)));
    }

    @Test
    public void testWithMultipleNumericDigitCharacter() {
        assertThat(isValid("1w3r5y"), is(equalTo(true)));
    }

    @Test
    public void testWithAllNumericDigitCharacter() {
        assertThat(isValid("12345"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithNoNumericDigitCharacter() {
        assertThat(isValid("QWErty"), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMaxIsLessThanMin() {
        NumericCharRuleValidator numericCharRuleValidatorTest = new NumericCharRuleValidator(0);
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = numericCharRuleValidator.validate("numeric", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
