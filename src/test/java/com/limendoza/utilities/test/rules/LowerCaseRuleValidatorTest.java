package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.LowerCaseRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class LowerCaseRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(LowerCaseRuleValidatorTest.class);

    // Test Positive
    @Test
    public void testWithSingleLowerCaseCharacter() {
        assertThat(isValid("QWErTY"), is(equalTo(true)));
    }

    @Test
    public void testWithMultipleLowerCaseCharacter() {
        assertThat(isValid("QwErTy"), is(equalTo(true)));
    }

    @Test
    public void testWithAllLowerCaseCharacter() {
        assertThat(isValid("qwerty"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithNoLowerCaseCharacter() {
        assertThat(isValid("QWERTY"), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMinIsZero() {
        LowerCaseRuleValidator lowerCaseRuleValidatorTest = new LowerCaseRuleValidator(0);
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = lowerCaseRuleValidator.validate("lowercase", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
