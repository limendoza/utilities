package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.validator.PasswordValidator;
import org.junit.Test;

import java.util.Collections;

public class PasswordValidatorTest extends UtilitiesApplicationTests {

    // Test Negative
    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithoutRuleValidators() {
        PasswordValidator passwordValidator = new PasswordValidator(Collections.emptyList());
    }

}
