package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.LengthRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class LengthRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(LengthRuleValidator.class);

    // Test Positive
    @Test
    public void testWithValidMinLength() {
        assertThat(isValid("qwert"), is(equalTo(true)));
    }

    @Test
    public void testWithValidMaxLength() {
        assertThat(isValid("qwertyuiopas"), is(equalTo(true)));
    }

    @Test
    public void testWithInBetweenMinMaxLength() {
        assertThat(isValid("qwertyu"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithInvalidMinLength() {
        assertThat(isValid("qwer"), is(equalTo(false)));
    }

    @Test
    public void testWithInvalidMaxLength() {
        assertThat(isValid("qwertyuiopasd"), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMaxIsLessThanMin() {
        LengthRuleValidator lengthRuleValidatorTest = new LengthRuleValidator(5, 4);
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = lengthRuleValidator.validate("length", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
