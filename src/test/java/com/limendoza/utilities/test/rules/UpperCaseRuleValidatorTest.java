package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.UpperCaseRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class UpperCaseRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpperCaseRuleValidatorTest.class);

    // Test Positive
    @Test
    public void testWithSingleUpperCaseCharacter() {
        assertThat(isValid("qweRty"), is(equalTo(true)));
    }

    @Test
    public void testWithMultipleUpperCaseCharacter() {
        assertThat(isValid("qWeRtY"), is(equalTo(true)));
    }

    @Test
    public void testWithAllUpperCaseCharacter() {
        assertThat(isValid("QWERTY"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithNoUpperCaseCharacter() {
        assertThat(isValid("qwerty"), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMinIsZero() {
        UpperCaseRuleValidator upperCaseRuleValidatorTest = new UpperCaseRuleValidator(0);
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = upperCaseRuleValidator.validate("uppercase", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
