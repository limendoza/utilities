package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SequenceRuleValidator extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SequenceRuleValidator.class);

    // Test Positive
    @Test
    public void testWithValidSequence() {
        assertThat(isValid("qwerty"), is(equalTo(true)));
    }

    @Test
    public void testWithCharInBetweenSequence() {
        assertThat(isValid("qwertyxqwerty"), is(equalTo(true)));
    }

    // Test Negative
    @Test
    public void testWithSameSequence() {
        assertThat(isValid("qwertyqwerty"), is(equalTo(false)));
    }

    @Test
    public void testWithCharFollowedBySameSequence() {
        assertThat(isValid("xqwertyqwerty"), is(equalTo(false)));
    }

    @Test
    public void testWithSameSequenceFollowedByChar() {
        assertThat(isValid("qwertyqwertyx"), is(equalTo(false)));
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = sequenceRuleValidator.validate("sequence", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
