package com.limendoza.utilities.test.rules;

import com.limendoza.utilities.UtilitiesApplicationTests;
import com.limendoza.utilities.dto.ValidationResultDTO;
import com.limendoza.utilities.validator.rules.InvalidCharRuleValidator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class InvalidCharRuleValidatorTest extends UtilitiesApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvalidCharRuleValidatorTest.class);

    // Test Positive
    @Test
    public void testWithNoInvalidCharacter() {
        assertThat(isValid("QWeRtY"), is(equalTo(true)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanceWithMinIsZero() {
        InvalidCharRuleValidator invalidCharRuleValidatorTest = new InvalidCharRuleValidator("");
    }

    // Test Negative
    @Test
    public void testWithSingleInvalidCharacter() {
        assertThat(isValid("QwE!Ty"), is(equalTo(false)));
    }

    @Test
    public void testWithMultipleInvalidCharacter() {
        assertThat(isValid("!w@r$y"), is(equalTo(false)));
    }

    @Test
    public void testWithAllInvalidCharacter() {
        assertThat(isValid("!@#$%^&*()"), is(equalTo(false)));
    }

    public boolean isValid(String word) {
        ValidationResultDTO results = invalidCharRuleValidator.validate("invalid", word);
        LOGGER.info(results.getError());

        return results.isValid();
    }
}
