package com.limendoza.utilities;

import com.limendoza.utilities.config.TestConfig;
import com.limendoza.utilities.validator.PasswordValidator;
import com.limendoza.utilities.validator.rules.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestConfig.class)
public abstract class UtilitiesApplicationTests {
	@Autowired
	protected PasswordValidator passwordRuleValidator;
	@Autowired
	protected InvalidCharRuleValidator invalidCharRuleValidator;
	@Autowired
	protected LengthRuleValidator lengthRuleValidator;
	@Autowired
	protected LowerCaseRuleValidator lowerCaseRuleValidator;
	@Autowired
	protected NumericCharRuleValidator numericCharRuleValidator;
	@Autowired
	protected SequenceRuleValidator sequenceRuleValidator;
	@Autowired
	protected SpecialCharRuleValidator specialCharRuleValidator;
	@Autowired
	protected UpperCaseRuleValidator upperCaseRuleValidator;
}
