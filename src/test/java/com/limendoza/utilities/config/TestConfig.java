package com.limendoza.utilities.config;

import com.limendoza.utilities.validator.PasswordValidator;
import com.limendoza.utilities.validator.intr.RuleValidator;
import com.limendoza.utilities.validator.rules.*;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@TestConfiguration
public class TestConfig {

    @Bean
    public PasswordValidator passwordRuleValidator() {
        List<RuleValidator> ruleValidators = new ArrayList<>();
        ruleValidators.add(new LengthRuleValidator(5, 12));
        ruleValidators.add(new LowerCaseRuleValidator(1));
        ruleValidators.add(new UpperCaseRuleValidator(1));
        ruleValidators.add(new NumericCharRuleValidator(1));
        ruleValidators.add(new SequenceRuleValidator());
        ruleValidators.add(new InvalidCharRuleValidator("!@#$%^&*()_+"));

        return new PasswordValidator(ruleValidators);
    }

    @Bean
    public InvalidCharRuleValidator invalidCharRuleValidator() {
        return new InvalidCharRuleValidator("!@#$%^&*()_+");
    }

    @Bean
    public LengthRuleValidator lengthRuleValidator() {
        return new LengthRuleValidator(5, 12);
    }

    @Bean
    public LowerCaseRuleValidator lowerCaseRuleValidator() {
        return new LowerCaseRuleValidator(1);
    }

    @Bean
    public NumericCharRuleValidator numericCharRuleValidator() {
        return new NumericCharRuleValidator(1);
    }

    @Bean
    public SequenceRuleValidator sequenceRuleValidator() {
        return new SequenceRuleValidator();
    }

    @Bean
    public SpecialCharRuleValidator specialCharRuleValidator() {
        return new SpecialCharRuleValidator(1,"!@#$%^&*()_+");
    }

    @Bean
    public UpperCaseRuleValidator upperCaseRuleValidator() {
        return new UpperCaseRuleValidator(1);
    }
}
